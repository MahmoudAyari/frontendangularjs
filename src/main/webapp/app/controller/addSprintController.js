'use strict';


agileApp.controller('addSprintCtrl', ['$scope', 'daoProjects', 'ngDialog','$rootScope',
                                  function($scope, daoProjects, ngDialog,$rootScope) {
	
	
		
		$scope.clickAddSprint = function () {
		        ngDialog.open({template: "view/addSprint.html", scope: $scope});
	       };
	
	       
	var addSprintOkCallback = function () {
	    daoProjects.getProjectSprints($scope.project.id, $scope.getProjectSprintsCallback);
		ngDialog.close();
		
	};
	
	$scope.validateAddSprint = function(sprintForm) {
		
		if (sprintForm != undefined) {
			if (sprintForm.name == undefined || sprintForm.name == "") {
				alert("Entrez un nom correct");
			} else if (sprintForm.startDate == undefined) {
				alert("Choisissez une date début");
			} else if (sprintForm.endDate == undefined) {
				alert("Choisissez une date fin");
			}else if (sprintForm.comment == undefined || sprintForm.comment == "") {
				alert("Entrez un commentaire correcte");
				
				
			} else {				
				daoProjects.addSprint(sprintForm.name,sprintForm.startDate.toUTCString(),sprintForm.endDate.toUTCString(),sprintForm.comment,$scope.project.id,addSprintOkCallback);
				
			}
		}
	};
	
	
	
}]);