'use strict';

agileApp.factory('daoProjects', function($http){
	
	var REST_API_ROOT_URL = 'http://localhost/';

	 return {
		 getAllProjects : function(callback){
           $http.get(REST_API_ROOT_URL + 'projects/all').success(callback);
         },
         
         getProjectSprints : function(idProject,callback){		
             $http.get(REST_API_ROOT_URL + 'sprints/project/'+idProject).success(callback);
         },
         getSprintTasks : function(idSprint,callback){
             $http.get(REST_API_ROOT_URL + 'tasks/sprint/'+idSprint).success(callback);
         },
         getSprint : function(idSprint,callback){
             $http.get(REST_API_ROOT_URL + 'sprints/'+idSprint).success(callback);
         },
         getTask : function(idTask,callback){
             $http.get(REST_API_ROOT_URL + 'tasks/'+idTask).success(callback);
         }, 
         addProject :  function(name,startDate,endDate,description, callback) {

        	 $http({
        		 method: 'POST',
        		 url: REST_API_ROOT_URL + 'projects/add',
        		 data: {
     		    	"name": name,
    		    	"startDate": startDate,
    		    	"endDate":endDate,
    		    	"description":description
    		    },
        		 headers: {
        		 'Content-type': 'application/json'
        		 }
        		 }).success(function(resp){
        		 callback(resp);
        		 }).error(function(){
        		 callback(undefined);
        		 });
        	 
        	 
        	 
         },
         updateTask :  function(id ,name,startDate,deadline,comment,etat,sprint,callback) {
              
          	 $http({
          		 method: 'PUT',
          		 url: REST_API_ROOT_URL + 'tasks/update',
          		 data: {
          			"id": id,
       		    	"name": name,
      		    	"startDate": startDate,
      		    	"deadline":deadline,
      		    	"comment":comment ,
      		    	"etat":etat,
      		    	"sprint":sprint
      		    },
          		 headers: {
          		 'Content-type': 'application/json'
          		 }
          		 }).success(function(resp){
          		 callback(resp);
          		 }).error(function(){
          		 callback(undefined);
          		 });
          	 
          	 
           }
         ,
         addSprint :  function(name,startDate,endDate,comment,idProject,callback) {

        	 $http({
        		 method: 'POST',
        		 url: REST_API_ROOT_URL + 'sprints/add/'+idProject,
        		 data: {
     		    	"name": name,
    		    	"startDate": startDate,
    		    	"endDate":endDate,
    		    	"comment":comment
    		    },
        		 headers: {
        		 'Content-type': 'application/json'
        		 }
        		 }).success(function(resp){
        		 callback(resp);
        		 }).error(function(){
        		 callback(undefined);
        		 });
        	 
        	 
         },
         addTask :  function(name,startDate,endDate,comment,idSprint,callback) {

        	 $http({
        		 method: 'POST',
        		 url: REST_API_ROOT_URL + 'tasks/add/'+idSprint,
        		 data: {
     		    	"name": name,
    		    	"startDate": startDate,
    		    	"deadline":endDate,
    		    	"comment":comment
    		    },
        		 headers: {
        		 'Content-type': 'application/json'
        		 }
        		 }).success(function(resp){
        		 callback(resp);
        		 }).error(function(){
        		 callback(undefined);
        		 });
        	 
        	 
         },
         updateSprint :  function(id ,name,startDate,endDate,comment,project,callback) {

        	 $http({
        		 method: 'PUT',
        		 url: REST_API_ROOT_URL + 'sprints/update',
        		 data: {
        			"id": id,
     		    	"name": name,
    		    	"startDate": startDate,
    		    	"endDate":endDate,
    		    	"comment":comment,
    		    	"project": project
    		    },
        		 headers: {
        		 'Content-type': 'application/json'
        		 }
        		 }).success(function(resp){
        		 callback(resp);
        		 }).error(function(){
        		 callback(undefined);
        		 });
        	 
        	 
         }
       
     };
       
     });
