'use strict';

var agileApp = angular.module('agileApp', ['ui.router', 'ngDialog', 'ngQuickDate', function ($httpProvider) {
	
}]);

agileApp.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
	
	// For any unmatched url, redirect to /projects
	$urlRouterProvider.otherwise("/projects");

	// Now set up the states
	$stateProvider.state('projects', {
		url: "/projects",
		templateUrl: "view/projects.html"
	}).state('projects.item', {
		url: "/{projectId}",
		templateUrl: "view/project.html"
	});
});