'use strict';


agileApp.controller('projectCtrl', ['$scope', '$stateParams', '$state', 'daoProjects', 'ngDialog',
                                  function($scope  ,$stateParams, $state, daoProjects, ngDialog) {
	
	$scope.getSprintTasksCallback = function(data) {
		$scope.clickedsprint1.tasks = data;
	};
	
	$scope.getProjectSprintsCallback = function(data) {
		$scope.project.sprints = data;
	};
	
	$scope.getClickedSprintCallback = function(data) {
		
		var d = new Date(data.startDate);
		data.startDate = d;
		var d = new Date(data.endDate);
		data.endDate = d;
		$scope.clickedsprint = data;
	};
	
   $scope.getClickedTaskCallback = function(data) {
		
		var d = new Date(data.startDate);
		data.startDate = d;
		var d = new Date(data.deadline);
		data.deadline = d;
		$scope.clickedTask = data;
	};
	
	
	$scope.$watch("loadProjectsOk", function(loadProjectsOk) {
		 
		if (loadProjectsOk) {
			var current_project = getProject(parseInt($stateParams.projectId), $scope.projects);
			
			if (current_project == null) {
				$state.go("projects");
			} else {
				
				$scope.project = current_project;
				$scope.setIdSelectedProject(current_project.id);
			    daoProjects.getProjectSprints($scope.project.id, $scope.getProjectSprintsCallback);
			     
				}
		}
	});
	
	var updateSprintOkCallback = function () {
	    daoProjects.getProjectSprints($scope.project.id, $scope.getProjectSprintsCallback);
		ngDialog.close();
		
	};
	
	var addTaskOkCallback = function () {
		daoProjects.getSprintTasks($scope.clickedsprint1.id,$scope.getSprintTasksCallback);
		ngDialog.close();
		
	};
	

	
	$scope.clickUpdateSprintButton = function(sprint) {
		 daoProjects.getSprint(sprint.id,$scope.getClickedSprintCallback);
		 ngDialog.open({template: "view/updateSprint.html", scope: $scope});
		 
		
	};
	
	

	$scope.clickUpdateTaskButton = function(task) {
		 daoProjects.getTask(task.id,$scope.getClickedTaskCallback);
		 ngDialog.open({template: "view/updateTask.html", scope: $scope});
		 
		
	};
	
	
	$scope.clickAddTask= function ( ) {
		 ngDialog.open({template: "view/addTask.html", scope: $scope});
   };

	
	
	$scope.clickTasksSprintButton = function(sprint) {
		$scope.clickedsprint1=sprint;
		 daoProjects.getSprintTasks($scope.clickedsprint1.id,$scope.getSprintTasksCallback);
		 
		
	};
 
	var updateTaskOkCallback = function () {
 	    daoProjects.getSprintTasks($scope.clickedsprint1.id, $scope.getSprintTasksCallback);
     	ngDialog.close();
		
	};
	
  $scope.validateUpdateSprint = function(sprintForm) {
		
		if (sprintForm != undefined) {
			if (sprintForm.name == undefined || sprintForm.name == "") {
				alert("Entrez un nom correct");
			} else if (sprintForm.startDate == undefined) {
				alert("Choisissez une date début");
			} else if (sprintForm.endDate == undefined) {
				alert("Choisissez une date fin");
			}else if (sprintForm.comment == undefined || sprintForm.comment == "") {
				alert("Entrez un commentaire correcte");
				
				
			} else {				
				daoProjects.updateSprint(sprintForm.id,sprintForm.name,sprintForm.startDate.toUTCString(),sprintForm.endDate.toUTCString(),sprintForm.comment,$scope.project,updateSprintOkCallback);
				 
			}
		}
	};
	
   $scope.validateUpdateTask = function(taskForm) {
		
		if (taskForm != undefined) {
			if (taskForm.name == undefined || taskForm.name == "") {
				alert("Entrez un nom correct");
			} else if (taskForm.startDate == undefined) {
				alert("Choisissez une date début");
			} else if (taskForm.deadline == undefined) {
				alert("Choisissez une date fin");
			}else if (taskForm.comment == undefined || taskForm.comment == "") {
				alert("Entrez un commentaire correcte");
				
				
			} else {
				
				daoProjects.updateTask(taskForm.id,taskForm.name,taskForm.startDate.toUTCString(),taskForm.deadline.toUTCString(),taskForm.comment,taskForm.etat,$scope.clickedsprint1,updateTaskOkCallback);
				 
			}
		}
	};
	

	
	  $scope.validateAddTask = function(taskForm) {
		
		if (taskForm != undefined) {
			if (taskForm.name == undefined || taskForm.name == "") {
				alert("Entrez un nom correct");
			} else if (taskForm.startDate == undefined) {
				alert("Choisissez une date début");
			} else if (taskForm.endDate == undefined) {
				alert("Choisissez une date fin");
			}else if (taskForm.comment == undefined || taskForm.comment == "") {
				alert("Entrez un commentaire correcte");
				
				
			} else {	
			  
				daoProjects.addTask(taskForm.name,taskForm.startDate.toUTCString(),taskForm.endDate.toUTCString(),taskForm.comment,$scope.clickedsprint1.id ,addTaskOkCallback);
				}
		}
	};
	
	var getProject = function(projectId, projects)  {
		if (isIndexOk(projectId)) {
			for (var i = 0; i < projects.length; i++) {
				if (projects[i].id == projectId) {
					return projects[i];
				}
			}
		}
		return null;
	};
	
	var isIndexOk = function(index) {
		if (typeof index === 'number') {
			if (index % 1 === 0 && index >= 0) {
				return true;
			}
		}
		return false;
	};
	
	
}]);