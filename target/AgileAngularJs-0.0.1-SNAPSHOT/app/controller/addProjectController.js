'use strict';

agileApp.controller('addProjectCtrl', ['$scope', 'ngDialog', 'daoProjects', '$rootScope',  function($scope, ngDialog, daoProjects, $rootScope) {
		
	$scope.validateAddProject = function(projectForm) {
		if (projectForm != undefined) {
			if (projectForm.name == undefined || projectForm.name == "") {
				alert("Entrez un nom correct");
			} else if (projectForm.startDate == undefined) {
				alert("Choisissez une date début");
			} else if (projectForm.endDate == undefined) {
				alert("Choisissez une date fin");
			}else if (projectForm.description == undefined || projectForm.description == "") {
				alert("Entrez une description correcte");
				
				
			} else {				
				daoProjects.addProject(projectForm.name,projectForm.startDate.toUTCString(),projectForm.endDate.toUTCString(), projectForm.description,  function () {
					ngDialog.close();
					$rootScope.$broadcast("reloadProjectsList");
				});
				
			}
		}
	};

}]);
