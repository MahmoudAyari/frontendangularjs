'use strict';



agileApp.controller('projectsCtrl', ['$scope', 'daoProjects',function ($scope, daoProjects){
	$scope.loadProjectsOk = false;
	$scope.idSelectedProject = undefined;
	var getAllProjects = function () {
	daoProjects.getAllProjects(function(data) {
	    $scope.projects = data;
	    $scope.loadProjectsOk =true;
	  });
	};
	getAllProjects ();
	
	$scope.$on("reloadProjectsList", getAllProjects);
	
	
	$scope.setIdSelectedProject = function (idProject) {
		$scope.idSelectedProject = idProject;
	};
	}]);